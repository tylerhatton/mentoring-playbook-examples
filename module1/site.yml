---
- name: Configure Infoblox
  hosts: ipam
  connection: local
  gather_facts: False
  vars:
    provider:
      host: "{{ inventory_hostname }}"
      username: "{{ ipam_username }}"
      password: "{{ ipam_password }}"
  tasks:
    - name: get a host record
      set_fact:
        host: "{{ lookup('nios', 'record:host', provider=provider, filter={'name': app_name+domain_name}) }}"
    - name: print host record
      debug:
        var: host
    - name: get a host record
      set_fact:
        reserved_ip: "{{ host['ipv4addrs'][0]['ipv4addr'] }}"
      when: host != []
    - name: Reserve new IP
      block:
        - name: Return next available IP for target network
          set_fact:
            reserved_ip: "{{ lookup('nios_next_ip', vip_network, provider=provider)[0] }}"
        - name: Configure an ipv4 host record
          nios_host_record:
            name: "{{ app_name }}{{ domain_name }}"
            ipv4:
              - address: "{{ reserved_ip }}"
            state: present
            provider: "{{ provider }}"
      when: host == []
      rescue:
      - name: Create an incident
        snow_record:
          username: "{{ snow_username }}"
          password: "{{ snow_password }}"
          instance: "{{ snow_instance }}"
          table: incident
          state: present
          data:
            assignment_group: 287ebd7da9fe198100f92cc8d1d2154e
            short_description: "Ansible Job Failed - {{ tower_job_id }}"
            work_notes: "Job {{ tower_job_id }} failed - could not create Infoblox Host record."
            severity: 3
            priority: 3
      - name: Fail the play
        fail:
          msg: "Unable to reserve host record"
    - name: print host record
      debug:
        msg: "Reserved IP is {{ reserved_ip }}"

- name: Configure bigips
  hosts: bigips
  connection: local
  gather_facts: False
  vars:
    provider:
      server: "{{ inventory_hostname }}"
      user: "{{ bigip_username }}"
      password: "{{ bigip_password }}"
      validate_certs: false
  tasks:
  - name: Set Pool name
    set_fact:
      pool_name: "{{ app_name }}{{ domain_name }}_pool"
  - name: Create F5 Pool
    bigip_pool:
      provider: "{{ provider }}"
      name: "{{ pool_name }}"
      lb_method: "round-robin"
      monitor_type: "and_list"
      monitors:
      - "/Common/tcp"
  - name: Assign Nodes to the Pool
    bigip_pool_member:
      provider: "{{ provider }}"
      pool: "{{ pool_name }}"
      description: "{{ item }}"
      address: "{{ item }}"
      port: "8080"
    with_items: "{{ groups['webservers'] }}"
  - name: Set VIP name
    set_fact:
      vip_name: "{{ app_name }}{{ domain_name }}_vs"
  - name: Create F5 Virtual Server
    bigip_virtual_server:
      provider: "{{ provider }}"
      name: "{{ vip_name }}"
      all_profiles:
        - http
      destination: "{{ hostvars['192.168.2.104']['reserved_ip'] }}"
      port: 80
      pool: "{{ pool_name }}"
      snat: Automap

- name: Run Health Checks
  hosts: bigips
  connection: local
  gather_facts: False
  vars:
    provider:
      server: "{{ inventory_hostname }}"
      user: "{{ bigip_username }}"
      password: "{{ bigip_password }}"
      validate_certs: false
  tasks:
    - name: Create F5 Virtual Server
      bigip_device_info:
        provider: "{{ provider }}"
        gather_subset:
          - virtual-servers
          - ltm-pools
      register: device_info
    - name: print host record
      debug:
        var: device_info
    - name: Get VIP info
      set_fact:
        vip_info: "{{ (device_info.virtual_servers | selectattr('name', 'search', vip_name) | list | first) }}"
    - name: Show vip status
      debug:
        msg: "{{ vip_info.availability_status }}"
    - name: Get Pool Info
      set_fact:
        pool_info: "{{ (device_info.ltm_pools | selectattr('name', 'search', pool_name) | list | first) }}"
    - name: Run configuration health checks
      assert:
        that:
          - vip_info.availability_status == "available"
          - pool_info.active_member_count == pool_info.available_member_count

